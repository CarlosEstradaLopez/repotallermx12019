var ClientesObtenidos;

function getClientes() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange=function(){
    if(this.readyState == 4 && this.status==200){
      //console.log(request.responseText);
      ClientesObtenidos = request.responseText;
      procesaClientes();
    }
  }

  request.open("GET",url,true);
  request.send();
}

function procesaClientes() {
  var rutaBandera ="https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var JSONClientes = JSON.parse(ClientesObtenidos);
  var divTabla = document.getElementById("tablaClientes");
  var tablaC=document.createElement("table");
  var tbodyC = document.createElement("tbody");

  tablaClientes.classList.add("table");
  tablaClientes.classList.add("table-striped");

  for(var i=0; i<JSONClientes.value.length;i++){
    //console.log(JSONProductos.value[i].ProductName);

    var nuevaFilaCliente = document.createElement("tr");

    var columnaFlag = document.createElement("td");
    var imgBandera = document.createElement("img");

    imgBandera.classList.add("flag");
    if(JSONClientes.value[i].Country == "UK"){
      imgBandera.src=rutaBandera + "United-Kingdom.png";
    }else{
      imgBandera.src=rutaBandera + JSONClientes.value[i].Country + ".png"
    }
    columnaFlag.appendChild(imgBandera);

    //columnaFlag.innerText = "<img src='https://www.countries-ofthe-world.com/flags-normal/flag-of-"+pais+".png' width='100px' height='100px'>";

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCity = document.createElement("td");
    columnaCity.innerText = JSONClientes.value[i].City;

    nuevaFilaCliente.appendChild(columnaNombre);
    nuevaFilaCliente.appendChild(columnaCity);
    nuevaFilaCliente.appendChild(columnaFlag);

    tbodyC.appendChild(nuevaFilaCliente);
  }
  tablaC.appendChild(tbodyC);

  divTabla.appendChild(tablaC);

}
